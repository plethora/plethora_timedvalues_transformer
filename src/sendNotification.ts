import * as https from 'https';
import { IncomingMessage } from 'http';

export default function sendNotification(data: any) {
    const oSToken = process.env.ONESIGNAL_TOKEN;

    var headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Basic ${oSToken}`,
    };

    var options = {
        host: 'onesignal.com',
        port: 443,
        path: '/api/v1/notifications',
        method: 'POST',
        headers: headers
    };

    var req = https.request(options, (res: IncomingMessage) => {
        res.on('data', (data: any) => {
            console.log('Response:');
            console.log(JSON.parse(data));
        });
    });

    req.on('error', function (e) {
        console.log('ERROR:');
        console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
};
