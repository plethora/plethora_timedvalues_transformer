import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();

if (error) {
    throw error;
}

import { IMessage } from '@plethora/types';
import { AmqpConnection, getPercentageThreshold } from '@plethora/utils';

import { Message } from 'amqplib';
import sendNotification from './sendNotification';
import { MongoClient, Db } from 'mongodb';

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};
const amqpConnection = new AmqpConnection(amqpConnectionConf);

const exchange = process.env.AMQP_EXCHANGE || 'plethora_topic';

const TOPIC_NAMES = (process.env.PLETHORA_MESSAGES || 'CPU_LOAD,RAM_LOAD')
    .split(',')
    .map(m => `plethora.${m}`);

let gDb: Db;

const handleError = (signal: string) => (err: any) => {
    console.error(err);
    if (gDb) {
        gDb.close(true)
            .then(() => {
                process.exit();
            });
    } else {
        process.exit();
    }
};

MongoClient.connect(process.env.MONGO_URL || 'mongodb://localhost:27017/PLETHORA')
    .then((db: Db) => {
        gDb = db;
        return;
    })
    .then(() => amqpConnection.retrieveTopicMessage(exchange, TOPIC_NAMES))
    .then((msgObservable: any) => {
        console.log('listen...');
        msgObservable
            .subscribe((msg: Message) => {
                const parsedMsg: IMessage = JSON.parse(msg.content.toString());
                console.log('Received message', parsedMsg);

                getPercentageThreshold(
                    gDb,
                    parsedMsg.device_id,
                    parsedMsg.module_id,
                )
                .then((threshold: number) => {
                    console.log('THRESHOLD: ', threshold);
                    if (parsedMsg.data.value > threshold) {
                        console.log('-- NOTIFICATION SENT!');
                        sendNotification({
                            app_id: process.env.APP_ID,
                            contents: {
                                // tslint:disable-next-line max-line-length
                                en: `module ${parsedMsg.module_id} reached a critical value of ${Number(parsedMsg.data.value).toFixed(2)}`,
                            },
                            included_segments: ['All'],
                        });
                    }
                })
                .catch((err: any) => {
                    console.error(err);
                });
            });
    })
    .catch(handleError);

['SIGINT', 'SIGQUIT', 'SIGTERM', 'uncaughtException']
    .forEach((signal: any) => {
        process.on(signal, handleError(signal));
    });
